/*
 * Copyright 2016 Pivotal Software, Inc..
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.davidsalter.exifviewer.web;

import com.davidsalter.exifviewer.domain.ExifTag;
import com.davidsalter.exifviewer.services.ExifExtractor;
import com.drew.imaging.ImageProcessingException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

/**
 *
 * @author David Salter <david.salter@outlook.com>
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class FileUploadControllerTests {

    @InjectMocks
    FileUploadController fileUploadController;

    @Mock
    ExifExtractor exifExtractor;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = standaloneSetup(fileUploadController).build();
    }

    @Test
    public void shouldFailIfInputFileIsEmpty() throws Exception {

        MockMultipartFile firstFile = new MockMultipartFile("file", "filename.txt", "text/plain", "".getBytes());
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/")
                .file(firstFile))
                .andExpect(flash().attributeExists("message"))
                .andExpect(flash().attributeCount(2))
                .andExpect(flash().attribute("message", equalTo("Failed to upload filename.txt because it was empty")));

    }

    @Test
    public void shouldReturnTagsFromImageFile() throws Exception {

        List<ExifTag> tags = new ArrayList<>();
        tags.add(new ExifTag("tag", "value"));
        when(exifExtractor.extractTags(any(File.class))).thenReturn(tags);

        MockMultipartFile firstFile = new MockMultipartFile("file", "filename.txt", "text/plain", "An Image".getBytes());
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/")
                .file(firstFile))
                .andExpect(flash().attributeExists("message"))
                .andExpect(flash().attributeCount(2))
                .andExpect(flash().attribute("message", equalTo("Successfully uploaded filename.txt")));
    }

    @Test
    public void shouldExitIfCannotParseImageFile() throws Exception {

        when(exifExtractor.extractTags(any(File.class))).thenThrow(ImageProcessingException.class);

        MockMultipartFile firstFile = new MockMultipartFile("file", "filename.txt", "text/plain", "Not An Image".getBytes());
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/")
                .file(firstFile))
                .andExpect(flash().attributeExists("message"))
                .andExpect(flash().attributeCount(2))
                .andExpect(flash().attribute("message", equalTo("Failed to parse the file filename.txt")));
    }

    @Test
    public void shouldExitIfCannotUploadImageFile() throws Exception {

        when(exifExtractor.extractTags(any(File.class))).thenThrow(IOException.class);

        MockMultipartFile firstFile = new MockMultipartFile("file", "filename.txt", "text/plain", "Not An Image".getBytes());
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/")
                .file(firstFile))
                .andExpect(flash().attributeExists("message"))
                .andExpect(flash().attributeCount(2))
                .andExpect(flash().attribute("message", equalTo("Failed to upload filename.txt")));
    }

}

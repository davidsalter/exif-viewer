/*
 * Copyright 2016 David Salter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.davidsalter.exifviewer.web;

import com.davidsalter.exifviewer.domain.ExifTag;
import com.davidsalter.exifviewer.services.ExifExtractor;
import com.drew.imaging.ImageProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Controller
public class FileUploadController {

    public static final String ROOT = "upload-dir";
    private static final String SUCCESSFUL_UPLOAD = "Successfully uploaded %s";
    private static final String FAILED_TO_PARSE = "Failed to parse the file %s";
    private static final String FAILED_TO_UPLOAD = "Failed to upload %s";
    private static final String EMPTY_FILE = "Failed to upload %s because it was empty";
    private static final String MESSAGE_KEY = "message";
    private static final String TAG_KEY = "tags";
    private static final String HTML_TEMPLATE = "index";

    private final ExifExtractor exifExtractor;

    @Autowired
    public FileUploadController(ExifExtractor exifExtractor) {
        this.exifExtractor = exifExtractor;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String provideUploadInfo(Model model) throws IOException {

        return HTML_TEMPLATE;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/")
    public String uploadFile(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {

        List<ExifTag> tags = new ArrayList<>();
        File imageFile = null;
        
        if (!file.isEmpty()) {
            try {
                Files.copy(file.getInputStream(), Paths.get(ROOT, file.getOriginalFilename()));
                redirectAttributes.addFlashAttribute(MESSAGE_KEY, String.format(SUCCESSFUL_UPLOAD, file.getOriginalFilename()));

                imageFile = new File(Paths.get(ROOT, file.getOriginalFilename()).toString());
                try {
                    tags = exifExtractor.extractTags(imageFile);
                } catch (ImageProcessingException e) {
                    redirectAttributes.addFlashAttribute(MESSAGE_KEY, String.format(FAILED_TO_PARSE, file.getOriginalFilename()));
                }
            } catch (IOException | RuntimeException e) {
                redirectAttributes.addFlashAttribute(MESSAGE_KEY, String.format(FAILED_TO_UPLOAD, file.getOriginalFilename()));
            }
        } else {
            redirectAttributes.addFlashAttribute(MESSAGE_KEY, String.format(EMPTY_FILE, file.getOriginalFilename()));
        }

        if (imageFile != null) {
            imageFile.delete();
        }

        redirectAttributes.addFlashAttribute(TAG_KEY, tags);
        
        return "redirect:/";
    }

}
